# Clojure

## Introduccion:

Clojure es un lenguaje de programación dinámico y de propósito general que combina la accesibilidad y el desarrollo interactivo de un lenguaje de script con una infraestructura eficiente y robusta para la programación multihilo. 

Clojure cuenta con las siguientes características:

- Es un lenguaje compilado, pero sigue siendo completamente dinámico: todas las características compatibles con Clojure son compatibles en tiempo de ejecución. 

- Proporciona fácil acceso a los frameworks de Java, debido a que este se ejecuta en la JVM.

- Es un dialecto de Lisp y comparte con Lisp la filosofía de código como datos y un poderoso sistema de macros. 

- Es predominantemente un lenguaje de programación funcional y cuenta con un conjunto rico de estructuras de datos inmutables y persistentes. 

## Sintaxsis basica

### Tipos de datos:

**Numericos**
```
5    ; Entero
-1.5 ; Punto flotante 
22/7 ; Ratio

```
Los numeros enteros por defectos se guardan en 64 bits de precision fija, en caso de que superen los 64 bits clojure automaticamente los guarda en 64bits de precision arbitraria.

[Precisión Arbitraria](https://es.wikipedia.org/wiki/Precisi%C3%B3n_arbitraria)



**Caracteres**
```
"Hola Conceptos"    ; string (Cadena)
\e                  ; character (Caracter)
#"[0-9]+"           ; expresion regular

```

Existen algunos caracteres reservados como: ```\newline \space \tab ```

Las expresiones regulares son cadenas (string) que comiensan con ```#```

**Símbolos e identifcadores**
```
+               ; Simbolo más
clojure.core/+  ; Se puede concatenar un simbolo a un namespace
nil             ; Valor nulo
true false      ; booleans
:alpha          ; Palabra clave
:release/alpha  ; Palabra clave con un namespace

```
Los simbolos estas compuestos por letras, numeros, y otras formas de puntuacion que son utilizadas para referir a otro cosa, como a una funcion, un valor, un namespace,etc. Los simbolos que poseen un namespace tiene que estar separados por una /.

Las palabras clave siempre inician con : y se evaluan a ellas mismas.

**Colecciones**

```
'(1 2 3)     ; listas
[1 2 3]      ; vectores
#{1 2 3}     ; set
{:a 1, :b 2} ; map

```
### Evaluación de expresion de Clojure

![Clojure Evaluation](./pictures/clojure-evaluation.png)

El codigo fuente es leido como caracteres por el [Reader](https://clojure.org/reference/reader). El Reader puede leer el codigo de archivos .clj o de una serie de expresiones dadas interactivamente. El Reader produce Clojure data que es tomada por el compilador de Clojure. Luego el compilador produce los binarios para la JVM

Aca hay dos puntos importantes:

- La unidad de codigo fuente no es un archivo de codigo fuente de clojure si no es una expresion de clojure. Debido a que el Reader lee los archivos .clj como una serie de expresiones interactivamente.
- La separacion entre el Reader y el compilador ya que esto da espacio a crear macros. Los macros son funciones especial que toman codigo (como data) y emiten codigo (como data).

### Estructura vs Semantica

Dada la siguiente expresion en clojure:

![Clojure Expresion](./pictures/clojure-expresion.png)

El diagrama ilustra la diferncia entre la sintaxsis (en verde) y la semantica (en azul). La mayoria de la expresiones de clojure se evaluan a allas mismas, excepto los simbolos y las listas. Los simbolos son utilizados para referir otra cosa y cuando evaluan cosas retornan la referencia de lo que evaluan. Las listas son evaluadas como invocaciones y se espera que contengan algo invocable.

En el diagrama, (+ 3 4) es leido como una lista que contiene el simbolo (+) y dos numeros (3 y 4). El primer elemento de la lista, donde se encuentra +, se puede llamar "posición de función" (function position). Esta posición es crucial porque determina qué se va a invocar.

Considering the evaluation of the expression above:

- 3 y 4 se evaluan a ellas mismas

- `+` evalua la funcion que implementa el +

- Evaluar la list va a invocar la funcion + con 3 y 4 como argumentos

En Clojure, todo es una expresion que evalua a un valor.

### Retrasar la Evaluación con Comillas

Clojure permite suspender la evaluación anteponiendo una comilla '. Esto es particularmente util en símbolos y listas que en algunas veces un símbolo debe ser solo un símbolo sin buscar a qué se refiere y una lista solo una lista de valores.

Ejemplo Símbolo:

```
user=> 'x
x
```
Ejemplo Lista:

```
user=> '(1 2 3)
(1 2 3)
```

Un error habitual es tratar de evaluar una lista que contiene solo datos:

```
user=> (1 2 3)
Execution error (ClassCastException) at user/eval156 (REPL:1).
class java.lang.Long cannot be cast to class clojure.lang.IFn
```
### REPL

La mayor parte del tiempo cuando estás utilizando Clojure, lo harás en un editor o en un REPL (Read-Eval-Print-Loop). El REPL tiene las siguientes partes:

1. El RELP lee una expresión (una cadena de caracteres) para producir datos de Clojure.
2. El RELP evalua los datos devueltos por el paso #1 para obtener un resultado (también datos de Clojure).
3. Imprimir el resultado convirtiéndolo de datos a caracteres.
4. Volver al inicio.

Un aspecto importante del paso #2 es que Clojure siempre compila la expresión antes de ejecutarla. 
Clojure siempre se compila a bytecode de JVM. No existe un intérprete de Clojure.

```
user=> (+ 3 4)
7
```
El codigo de arriba demuestra la evaluación de una expresión (+ 3 4) y la recepción de un resultado utilizando un RELP.

### RELP +

La mayoría de los entornos REPL admiten algunos trucos para ayudar con el uso interactivo. Por ejemplo, algunos símbolos especiales guardan los resultados de la evaluación de las últimas tres expresiones:

. `*1` (el último resultado)
. `*2` (el resultado de dos expresiones atrás)
. `*3` (el resultado de tres expresiones atrás)

```
user=> (+ 3 4)
7
user=> (+ 10 *1)
17
user=> (+ *1 *2)
24
```
Además, hay un namespace reservado clojure.repl que se incluye en la biblioteca estándar de Clojure y que proporciona una serie de funciones útiles. Para cargar esa biblioteca y hacer que sus funciones estén disponibles en nuestro contexto actual, ejecuta:

```
(require '[clojure.repl :refer :all])

```
Ahora tenemos acceso a algunas funciones adicionales que son útiles en el REPL: doc, find-doc, apropos, source, y dir.

**Funcion doc**

La función doc muestra la documentación de cualquier función. Ejemplo la funcion +:

```
user=> (doc +)

clojure.core/+
([] [x] [x y] [x y & more])
  Returns the sum of nums. (+) returns 0. Does not auto-promote
  longs, will throw on overflow. See also: +'

```
La función doc imprime la documentación de +, incluyendo las firmas válidas.

También podemos invocar doc sobre sí misma:

```
user=> (doc doc)

clojure.repl/doc
([name])
Macro
  Prints documentation for a var or special form given its name

```
**Función apropos**

¿No estás seguro de cómo se llama algo? Puedes usar el comando apropos para encontrar funciones que coincidan con una cadena en particular o una expresión regular.

```
user=> (apropos "+")
(clojure.core/+ clojure.core/+')

```

**Función find-doc**

También puedes ampliar tu búsqueda para incluir las cadenas de documentación con find-doc:

```
user=> (find-doc "trim")

clojure.core/subvec
([v start] [v start end])
  Returns a persistent vector of the items in vector from
  start (inclusive) to end (exclusive).  If end is not supplied,
  defaults to (count vector). This operation is O(1) and very fast, as
  the resulting vector shares structure with the original and no
  trimming is done.

clojure.string/trim
([s])
  Removes whitespace from both ends of string.

clojure.string/trim-newline
([s])
  Removes all trailing newline \n or return \r characters from
  string.  Similar to Perl's chomp.

clojure.string/triml
([s])
  Removes whitespace from the left side of string.

clojure.string/trimr
([s])
  Removes whitespace from the right side of string.

```
**Función dir**

Si deseas ver una lista completa de las funciones en un namespace en particular, puedes usar la función dir. Aquí podemos usarla en el namespace clojure.repl:

```
user=> (dir clojure.repl)

apropos
demunge
dir
dir-fn
doc
find-doc
pst
root-cause
set-break-handler!
source
source-fn
stack-element-str
thread-stopper

```

**Función source**

Finalmente, podemos ver no solo la documentación sino también el código fuente subyacente de cualquier función accesible por el tiempo de ejecución:

```
user=> (source dir)

(defmacro dir
  "Prints a sorted directory of public vars in a namespace"
  [nsname]
  `(doseq [v# (dir-fn '~nsname)]
     (println v#)))
```

Si desea ver mas informacion sobre las librerias que ofrece Clojure puede acerlo haciendo [click aqui](https://clojure.org/api/cheatsheet)

### Clojure mas definiciones basicas

#### def

Cuando estás evaluando cosas en un REPL, puede ser útil guardar un dato para más tarde. Podemos hacer esto con def:

```
user=> (def x 7)
#'user/x
```
def es una forma especial que asocia un símbolo (x) en el namespace actual con un valor (7). Esta vinculación se llama una var (variable). En la mayoría del código real de Clojure, las vars deben referirse a un valor constante o una función, pero es común definir y redefinirlos para mayor comodidad cuando se trabaja en el REPL.

Recuerda que los símbolos se evalúan buscando a qué se refieren, por lo que podemos obtener el valor de vuelta simplemente usando el símbolo:

```
user=> (+ x x)
14
```
#### Imprimir (print)

Una de las cosas más comunes que haces al aprender un lenguaje es imprimir valores. Clojure proporciona varias funciones para imprimir valores:

| Para humanos      | Legible como datos |
| :---------------- | :-----------------:|
| Con salto de línea|                    |
| 'println'         |   prn              |
| sin salto de línea|                    |
| 'print'           |  pr                |

Las formas legibles para humanos traducirán caracteres especiales de impresión (como saltos de línea y tabulaciones) a su forma impresa y omitirán las comillas en las cadenas. A menudo usamos println para depurar funciones o imprimir un valor en el REPL. println toma cualquier número de argumentos e interpone un espacio entre el valor impreso de cada argumento:

```
user=> (println "What is this:" (+ 1 2))
What is this: 3

```

La función println tiene efectos secundarios (impresión) y devuelve nil como resultado.

Nota que "What is this:" arriba no imprimió las comillas circundantes y no es una cadena que el lector podría leer nuevamente como datos.

Para ese propósito, usa prn para imprimir como datos:
```
user=> (prn "one\n\ttwo")
"one\n\ttwo"
```

Ahora el resultado impreso es una forma válida que el lector podría leer nuevamente. Dependiendo del contexto, puedes preferir la forma humana o la forma de datos.

### Continuación...

En los siguientes enlaces encontras informacion acerca de las [instalaciones necesarias](./guides/instalation.md) para empezar a programar en clojure y algunos [ejercicios basicos](./exercises/Readme.md)

### Copyrigth form

El contenido detallado en el [Readme.md](./Readme.md) son traducciones e interpretaciones de los tutoriales que se encuentran en: https://clojure.org/guides/learn/




