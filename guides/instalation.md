### Introduccion - Clojure CLI y CLJ :

Clojure (el lenguaje) se proporciona como un archivo Java ARchive (JAR), disponible en el [Repositorio Central de Maven](https://mvnrepository.com/artifact/org.clojure/clojure).

A continuacion encontrar informacion sobre como instalar la herramienta de línea de comandos (el CLI de Clojure). Esta herramienta es importante para compilar codigo de clojure, util para instalar herramientas de clojure, correr programas de clojure, etc.

#### Instruciones para MAC OS

Requisitos previos: [Java](https://www.oracle.com/ar/java/technologies/downloads/), [brew](https://brew.sh/)

Este comando agrega clojure y clj a tu sistema:
```
brew install clojure/tools/clojure
```

Si ya tiene clojure instalado, podes utilizar este comando para actualizarlo:
```
brew upgrade clojure/tools/clojure
```

#### Instruciones para Linux


Requisitos previos: [Java](https://www.oracle.com/ar/java/technologies/downloads/), bash, [curl](https://curl.se/) y [rlwrap](https://linux.die.net/man/1/rlwrap)


Para instalar con el instalador de script de Linux:

Antes de proceder con la instalacion es necesario que esten instaladas las dependencias mencionadas.

Utiliza el script linux-install para descargar y ejecutar la instalación, que creará los ejecutables /usr/local/bin/clj, /usr/local/bin/clojure y el directorio /usr/local/lib/clojure:

```
curl -L -O https://github.com/clojure/brew-install/releases/latest/download/linux-install.sh
chmod +x linux-install.sh
sudo ./linux-install.sh
```
Para instalar en una ubicación personalizada (como /opt/infraestructura/clojure), usa la opción --prefix:

```
sudo ./linux-install.sh --prefix /opt/infraestructura/clojure

```
También puedes querer ampliar el MANPATH en /etc/man_db.conf para incluir las páginas del manual:

```
MANPATH_MAP /opt/infraestructura/clojure/bin /opt/infraestructura/clojure/man
```

Por ultimo el script linux-install se puede eliminar:

```
rm linux-install.sh
```

#### Instruciones para Windows:
Requisitos previos: [Java](https://www.oracle.com/ar/java/technologies/downloads/).

Podes encontrar las instrucciones de como instalar clj en Windows en el siguiente [enlace](https://github.com/clojure/tools.deps.alpha/wiki/clj-on-Windows)


### Editores

Para obtener una mejor experiencia con clojure se recomienda utilizar un editor que admita la evaluación de codigo (a través de un REPL conectado). Los editores recomendados son:

#### IntelliJ

[IntelliJ IDEA](https://www.jetbrains.com/idea/) es uno de los principales IDE "modernos" con soporte para una amplia variedad de lenguajes y herramientas. La Edición Comunitaria de IntelliJ IDEA para desarrollo JVM está disponible como descarga gratuita.

Los complementos recomendados para el desarrollo de Clojure en IntelliJ son:

- [Cursive](https://cursive-ide.com/) - un entorno de desarrollo completo para Clojure.
- [Clojure-extras](https://plugins.jetbrains.com/plugin/18108-clojure-extras/) - integra funcionalidades extras como resultados de evaluación en línea y verificación de estilo a través de clj-kondo.

**Cursive** es una buena opción si ya estás familiarizado con Java o esperas trabajar con proyectos mixtos de Clojure/Java.

#### VS Code

Visual Studio Code es un editor de código fuente desarrollado por Microsoft para Windows, Linux, macOS y Web. Incluye soporte para la depuración, control integrado de Git, resaltado de sintaxis, finalización inteligente de código, fragmentos y refactorización de código.

Se recomienda instalar el complemento [Calva](https://calva.io/) que provee un ambiente interactivo para desarrollar en Clojure.

#### Vim y Neovim

Vim es un editor con una larga historia, conocido por su capacidad para editar texto de manera rápida y eficiente.

Su interfaz de usuario única y poderosa es lo suficientemente popular como para que la mayoría de los otros editores mencionados anteriormente incluyan modos vi que reproducen la funcionalidad principal de la interfaz de usuario de Vim.

Neovim es un fork de Vim. Aunque mantiene un alto grado de compatibilidad, hay algunas diferencias significativas entre los dos editores.

Tanto Vim como Neovim son altamente extensibles y hay una vasta gama de complementos disponibles para ellos. La mayoría, pero no todos, los complementos funcionarán con ambos editores.

Los siguientes complementos proporcionan soporte para Clojure tanto para Vim como para Neovim:

- [Fireplace](https://github.com/tpope/vim-fireplace) - soporte de Clojure e integración de REPL.
- [vim-iced](https://liquidz.github.io/vim-iced/) - inspirado en CIDER.
- [vim-slamhound](https://github.com/guns/vim-slamhound) - reescritura automática de espacios de nombres.

Los siguientes complementos solo proporcionan soporte de Clojure para Neovim y sera el utilizado en los ejemplos de este repositorio:

- [Conjure](https://github.com/Olical/conjure) - enfoque en desarrollo interactivo.

