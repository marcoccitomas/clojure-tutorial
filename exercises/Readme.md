Ejemplo de prolog
===============

Algunos ejemplos de prolog con su equivalente en clojure.

**Aclaración:** Al ser dos lenguages con paradigmas completamente diferentes algunos ejercicios tiene algunos trucos al ser adaptados de prolog a clojure.


###  detective problem

El ejercicio del detective consiste en interrogar a 3 testigos y determinar quien es el sospechoso.

Solucion en [prolog](./1-Detective.pl)

Solucion en [clojure](./1-Detective.clj)


### talespin2.pl

Implementación de un generador de historias.

Solucion en [prolog](./2-talespin.pl)

Solucion en [clojure](./2-talespin.clj)

### tictactoe.pl

Implementación de tic tac 


Solucion en [prolog](./3-tictactoe.pl)

Solucion en [clojure](./3-tictactoe.clj)

### birds.pl

El ejemplo de los pájaros del tutorial 'sistemas expertos en Prolog' de Amzi (en la web)

Solucion en [prolog](./4-birds.pl)

Solucion en [clojure](./4-birds.clj)

### cannibals2.pl

Un ejemplo bien comentado del problema de los caníbales y misioneros.

Solucion en [prolog](./5-cannibals2.pl)

Solucion en [clojure](./5-cannibals2.clj)

### cuttutorial.pl

Una pequeña historia analógica para ayudar a entender "cut" y "cut, fail".

Solucion en [prolog](./6-cuttutorial.pl)

Solucion en [clojure](./6-cuttutorial.clj)

### emoticons.pl

Un programa lleno de emoticons

Solucion en [prolog](./7-emoticons.pl)

Solucion en [clojure](./7-emoticons.clj)


### addlists.pl

Restringe dos listas para que sumen par a par a una tercera.


Solucion en [prolog](./8-addlist.pl)

Solucion en [clojure](./8-addlist.clj)

### children.pl

Un problema completamente resuelto que implica sentar a los niños en un aula.

Solucion en [prolog](./9-children.pl)

Solucion en [clojure](./9-children.clj)


### loops.pl

Demostración de varias formas de hacer en Prolog lo que harías con bucles en un lenguaje imperativo.

Solucion en [prolog](./10-loops.pl)

Solucion en [clojure](./10-loops.clj)

### sudoku.pl

Resuelve un puzzle de Sudoku utilizando CLPFD.


Solucion en [prolog](./12-sudoku.pl)

Solucion en [clojure](./sudoku)

### nqueens.pl

Implementación de la versión generalizada del clásico problema de las 8 reinas.

Solucion en [prolog](./11-nqueens.pl)

Solucion en [clojure](./nqueen)

### techtree2.pl

Resolver problemas de "árbol tecnológico" de juegos utilizando CHR.

Solucion en [prolog](./13-techtree2.pl)

Solucion en [clojure](./tech-tree)








