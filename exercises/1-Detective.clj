; a, b y c representan a Art, Burt y Carl

; Primero vamos a definir los testimonios de los acusados

;Para ello utilizamos un mapa donde cada clave representa un testigo (:a, :b, :c) y el valor es un conjunto de declaraciones. Cada declaración es una lista donde el primer elemento es el tipo de declaración (:friend, :enemy, :out-of-town, :in-town, :stranger) y el segundo elemento es el sujeto mencionado.

(def testimony
  {:a #{[:friend :b] [:enemy :c]}
   :b #{[:out-of-town :b] [:stranger :b]}
   :c #{[:in-town :c] [:in-town :a] [:in-town :b]}})

; Ahora definimos las inconsistencias. Por ejemplo que no pueden ser amigos y enemigos al mismo tiempo.

; La siguite función verifica si dos testimonios t1 y t2 son inconsistentes según las reglas establecidas. Utiliza cond para evaluar cada caso posible de inconsistencia y devuelve true si alguna regla se cumple, o false si no hay inconsistencia.

(defn inconsistent [t1 t2]
  (cond
    (and (= (first t1) :friend) (= (first t2) :enemy) (= (second t1) (second t2))) true
    (and (= (first t1) :friend) (= (first t2) :stranger) (= (second t1) (second t2))) true
    (and (= (first t1) :enemy) (= (first t2) :stranger) (= (second t1) (second t2))) true
    (and (= (first t1) :out-of-town) (= (first t2) :in-town) (= (second t1) (second t2))) true
    :else false))

; Ahora escribimos la función para verificar si algún testimonio es inconsistente


(defn inconsistent-testimony? [witnesses]
  (some (fn [x]
          (some (fn [y]
                  (and (not= x y)
                       (some (fn [tx] (some (fn [ty] (inconsistent tx ty)) (testimony y))) (testimony x))))
                witnesses))
        witnesses))


;Esta función toma una lista de testigos witnesses y verifica si hay algún testimonio inconsistente entre ellos. Utiliza some para iterar sobre cada testigo x y luego compara su testimonio con todos los otros testigos y. Se asegura de que x e y sean diferentes ((not= x y)) y luego verifica la inconsistencia llamando a la función inconsistent.

;La función some en Clojure es una función de orden superior que se utiliza para determinar si al menos un elemento de una colección cumple con una condición específica


; Esta función toma una lista de testigos witnesses y verifica si hay algún testimonio inconsistente entre ellos.

(defn consistent? [witnesses]
  (not (inconsistent-testimony? witnesses)))

;Esta función simplemente verifica si no hay testimonios inconsistentes entre los testigos dados. Devuelve true si son consistentes y false si hay al menos un testimonio inconsistente.


(defn murderer []
  (let [witnesses [:a :b :c]
        suspects witnesses
        consistent-suspects (filter #(consistent? (remove #{%} witnesses)) suspects)]
    consistent-suspects))

; Esta función crea una lista de sospechosos (suspects), luego filtra aquellos sospechosos cuyos testimonios son consistentes eliminando su propio testimonio de la lista de testigos y evaluando la consistencia con consistent?.

;; Imprimo los sospechosos:
(println "Posibles sospechosos:" (murderer))

