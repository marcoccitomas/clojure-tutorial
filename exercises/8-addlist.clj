(ns constraints.core
  (:require [clojure.core.logic :refer [run* fresh == conde lvar]]
            [clojure.core.logic.fd :as fd]))

;;
;; Demostración simple que restringe una lista a ser la suma por pares de otras
;; dos listas
;;

;; Función que suma dos listas elemento por elemento y restringe la tercera lista a ser la suma de las dos primeras
(defn addlists [a b c]
  (conde
    [(== a []) (== b []) (== c [])]
    [(fresh [ha ta hb tb hc tc]
       (== [ha & ta] a)
       (== [hb & tb] b)
       (== [hc & tc] c)
       (fd/+ ha hb hc)
       (addlists ta tb tc))]))

;; Ejemplo de uso
(defn example []
  (run* [q]
    (fresh [a b c]
      (== a [1 2 3])
      (== b [4 5 6])
      (addlists a b c)
      (== q c))))

;; Ejecutar el ejemplo
(println (example))

