(ns birds
  (:gen-class))

;; Declaración de hechos dinámicos
(defonce known (atom #{}))
(defonce voice (atom #{}))
(defonce season (atom #{}))
(defonce cheek (atom #{}))
(defonce head (atom #{}))
(defonce flight (atom #{}))
(defonce bill (atom #{}))
(defonce live (atom #{}))
(defonce nostrils (atom #{}))

;; Definición de aves y características
(defn bird [x]
  (case x
    :laysan_albatross (and (family :albatross)
                           (color :white))
    :black_footed_albatross (and (family :albatross)
                                  (color :dark))
    :whistling_swan (and (family :swan)
                         (voice :muffled_musical_whistle))
    :trumpeter_swan (and (family :swan)
                         (voice :loud_trumpeting))
    :canada_goose (or (and (family :goose)
                           (season :winter)
                           (country :united_states)
                           (head :black)
                           (cheek :white))
                      (and (family :goose)
                           (season :summer)
                           (country :canada)
                           (head :black)
                           (cheek :white)))))

;; Definición de características de aves y condiciones
(defn family [x]
  (case x
    :albatross (and (order :tubenose)
                    (size :large)
                    (wings :long_narrow))
    :swan (and (order :waterfowl)
               (neck :long)
               (color :white)
               (flight :ponderous))
    :goose (family :waterfowl)))

(defn order [x]
  (case x
    :tubenose (and (nostrils :external_tubular)
                   (live :at_sea)
                   (bill :hooked))
    :waterfowl (and (feet :webbed)
                    (bill :flat))))

(defn country [x]
  (case x
    :united_states (or (region :mid_west)
                       (region :south_west)
                       (region :north_west)
                       (region :mid_atlantic))
    :canada (or (province :ontario)
                (province :quebec))))

(defn region [x]
  (case x
    :new_england (state :massachusetts :vermont)
    :south_east (state :florida :mississippi)))

(defn state [x]
  (contains? #{:florida :mississippi :massachusetts :vermont} x))

(defn province [x]
  (contains? #{:ontario :quebec} x))

;; Funciones de interacción con el usuario
(defn ask [a v]
  (if-let [known-value (get-in @known [a v])]
    (if (= known-value :yes)
      true
      false)
    (let [answer (read-string (str a ": " v "? "))]
      (swap! known update-in [a v] (constantly (if (= answer "yes") :yes :no)))
      (= answer "yes"))))

(defn multivalued? [x]
  (contains? #{:voice :feed} x))

(defn eats [x] (ask :eats x))
(defn feet [x] (ask :feet x))
(defn wings [x] (ask :wings x))
(defn neck [x] (ask :neck x))
(defn color [x] (ask :color x))
(defn size [x] (menuask :size x #{:large :plump :medium :small}))
(defn flight [x] (menuask :flight x #{:ponderous :agile :flap_glide}))

(defn menuask [a v menu-list]
  (println (str "¿Cuál es el valor para " a "?"))
  (println menu-list)
  (let [x (read-line)]
    (if (contains? menu-list (keyword x))
      (do
        (swap! known update-in [a] (constantly x))
        (= x v))
      (do
        (println (str x " no es un valor válido, intenta nuevamente."))
        (recur a v menu-list)))))

;; Función principal para resolver
(defn top-goal [x] (bird x))

(defn solve []
  (reset! known {})
  (let [result (first (filter top-goal [:laysan_albatross :black_footed_albatross
                                         :whistling_swan :trumpeter_swan :canada_goose]))]
    (if result
      (println (str "La respuesta es " result))
      (println "No se encontró ninguna respuesta."))))


