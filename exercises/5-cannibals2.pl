%
% Caníbales y misioneros.
% Comenzando con alguna distribución de caníbales y misioneros
% en las orillas de un río, y una canoa que puede llevar a 2 personas,
% mover a todos a la orilla derecha del río.
%
% Definir el caso 'base' - cuando hemos terminado.
cannibals(_, 0,0,_,_, _, Lista, Lista).

% cannibals(
%           +Orilla,  -1 la canoa está en la orilla izquierda 1 la canoa está en la orilla derecha
%           +CanibalesEnOrillaIzquierda,
%           +MisionerosEnOrillaIzquierda,
%           +CanibalesEnOrillaDerecha,
%           +MisionerosEnOrillaDerecha,
%           +Visitados  - una lista de estados ya visitados.
%               Cada estado es de la forma canoe(B,C,M),
%                  donde B es -1 (orilla izquierda) o 1 (orilla derecha)
%                     C es caníbales en la orilla izquierda,
%                     M es misioneros en la orilla izquierda.
%           +Lista - la lista en orden inverso de movimientos completos
%                     de la forma go(B, C, M)
%                     donde B es 1 yendo a la derecha y -1 yendo a la izquierda
%                     y C y M son el número de caníbales y
%                         misioneros en la canoa
%           -ListaCompleta) - la solución completa en el mismo formato
%                             que Lista.

cannibals(Orilla, LC,LM,RC,RM, Visitados, Lista, ListaCompleta) :-
    canoeCarries(DC, DM),
    % hey, eso es un bucle, ¡más o menos! bienvenido al backtracking!

    % en prolog las variables no varían, definimos nuevas para cómo
    % cuántos de qué están en qué orillas después del movimiento
    % el 'is' hace matemáticas de verdad. Cuidado, = NO hace
    % asignación!
    NLM is (LM + Orilla * DM),
    NRM is RM - Orilla * DM,
    NLC is LC + Orilla * DC,
    NRC is RC - Orilla * DC,
    % y nos aseguramos de que los caníbales no superen en número a los misioneros
    % o que no haya misioneros para comer en la orilla izquierda
    (NLC =< NLM ; NLM == 0),
    % lo mismo para la orilla derecha
    (NRC =< NRM ; NRM == 0),
    % y no queremos tener números negativos de nadie en ningún lugar
    NLC >= 0,
    NLM >= 0,
    NRC >= 0,
    NRM >= 0,
    NOrilla is Orilla * -1,
    % y no estamos repitiendo nuestro estado anterior
    \+ memberchk(canoe(NOrilla, NLC, NLM), Visitados),
    % y ahora tenemos que tener una solución desde la nueva posición
    cannibals(NOrilla,
              NLC, NLM, NRC, NRM,
              [canoe(NOrilla, NLC, NLM) | Visitados],
              [go(NOrilla, DC, DM) | Lista], ListaCompleta).

% ahora simplemente enumeramos los posibles contenidos de la canoa
%
% canoeCarries(?CanibalesEnCanoa, ?MisionerosEnCanoa)
canoeCarries(1,0).
canoeCarries(0,1).
canoeCarries(1,1).
canoeCarries(2,0).
canoeCarries(0,2).

% método de conveniencia para probar
go :-
    cannibals(-1, 3, 3, 0, 0, [canoe(-1,3,3)], [], Completa),
    reverse(Completa, NC),
    showsolution(3, 3, 0, 0, NC).

% showsolution(
%        al inicio tenemos:
%      +CanibalesEnOrillaIzquierda,
%      +MisionerosEnOrillaIzquierda,
%      +CanibalesEnOrillaDerecha,
%      +MisionerosEnOrillaDerecha,
%      +Lista)  lista de movimientos para imprimir

showsolution(_, _, _, _, []).
showsolution(C, M, RC, RM, [go(-1, DC, DM) | Sol]) :-
    format('~d,~d   \\_______/   ~d,~d~n',
           [C, M, RC, RM]),
    format('    <-(~d,~d)--~n', [DC, DM]),
    NLC is C + DC,
    NLM is M + DM,
    NRC is RC - DC,
    NRM is RM - DM,
    showsolution(NLC, NLM, NRC, NRM, Sol).

showsolution(C, M, RC, RM, [go(1, DC, DM) | Sol]) :-
    format('~d,~d   \\_______/   ~d,~d~n',
           [C, M, RC, RM]),
    format('       --(~d,~d)->~n', [DC, DM]),
    NLC is C - DC,
    NLM is M - DM,
    NRC is RC + DC,
    NRM is RM + DM,
    showsolution(NLC, NLM, NRC, NRM, Sol).

