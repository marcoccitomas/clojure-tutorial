(ns tech-tree.core
  (:require [clojure.set :refer [intersection subset?]]
            [clojure.pprint :refer [pprint]]))

;; Definición de las necesidades iniciales del juego
(def initial-needs
  {:barracks #{:peasant}
   :stable #{:peasant :barracks}
   :dock #{:peasant :barracks}
   :swordsman #{:barracks}
   :knight #{:stable}
   :boat #{:dock :peasant}})

;; Almacén para registrar qué unidades ha visto el jugador
(defonce state (atom #{}))

;; Predicado para registrar que se ha visto una unidad
(defn i-saw [unit]
  (swap! state conj unit))

;; Función para verificar si se han cumplido todas las necesidades para construir una unidad
(defn can-build? [unit]
  (every? (fn [need] (subset? (set [need]) @state)) (initial-needs unit)))

;; Función para calcular las unidades que el oponente puede construir
(defn possible-builds []
  (let [buildable (filter can-build? (keys initial-needs))]
    (map #(keyword (name %)) buildable)))

;; Función para reiniciar el estado del juego
(defn reset-state []
  (reset! state #{}))

;; Imprimir el estado actual de las construcciones y las construcciones posibles
(defn print-store []
  (println "================")
  (when (seq @state)
    (println "Tu enemigo ha construido:" (seq @state)))
  (let [builds (possible-builds)]
    (when (seq builds)
      (println "Tu enemigo puede construir:" builds))))

;; Ejemplo de uso inicial
(defn -main [& args]
  (reset-state)
  (doseq [unit args]
    (i-saw (keyword unit)))
  (print-store))

;; Ejemplo de inicialización con dos campesinos
(defn init []
  (reset-state)
  (doseq [unit [:peasant :peasant]]
    (i-saw unit))
  (print-store))

