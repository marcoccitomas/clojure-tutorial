El programa [**talespin.pl**](./2-talespin.pl) es una reconstrucción simplificada del generador de historias TALE-SPIN de James Meehan, adaptado para generar "historias" de incidentes de aviación. Utiliza un conjunto de reglas y definiciones en Prolog para simular la planificación y ejecución de acciones en un escenario de vuelo. Aquí hay un resumen de sus principales componentes y funcionalidades:

1. Planificador Simili-STRIPS:

 - Implementa un planificador de encadenamiento hacia atrás sin una lista de objetivos protegida (**make_best_plan/3**).
 - Se define mediante reglas y hechos que representan acciones posibles como cargar pasajeros, despegar, aterrizar, etc.
 - Calcula la calidad de los planes en función de su longitud y costos asociados.

2. Simulador de Incidentes:

 - Similar al planificador, pero también puede introducir incidentes aleatorios que requieren replanificación.
 - Los incidentes pueden incluir emergencias como incendios en el motor o pasajeros enfermos.
 - Requiere gestionar estos eventos imprevistos durante la ejecución del plan.

3. Definiciones de Eventos:

 - Cada acción y suceso están definidos con precondiciones, efectos (eliminaciones y adiciones) y texto descriptivo en inglés.
 - Las acciones rutinarias incluyen movimientos del avión, mientras que las de emergencia cubren evacuaciones y aterrizajes de emergencia.

4. Reglas y Hechos:

 - Utiliza reglas lógicas y hechos para definir cómo se afecta el estado del mundo (situación del vuelo) por las acciones y sucesos.
 - Las reglas ayudan a determinar si se ha satisfecho un objetivo o si se cumplen las condiciones para una acción específica.

5. Ejecución del Programa:

 - El programa se ejecuta desde el intérprete de Prolog (SWI-Prolog).
 - Carga el archivo talespin.pl en el intérprete usando [talespin]..
 - Luego, se llama al predicado principal talespin/0 para iniciar la generación de la historia de incidente de aviación.
