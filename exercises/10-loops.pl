%
% Una demostración de varias formas de hacer un bucle
%

%
% Recursión
%
recurse([]).
recurse([H|T]) :-
	writeln(H),
	recurse(T).

%
% Bucle impulsado por fallo
%
fail_driven(List) :-
	member(X, List),
	writeln(X),
	fail.
fail_driven(_).

%
% Map
%
by_map(List) :-
	maplist(writeln, List).

%
% Usar repeat para generar un número infinito de puntos de elección
% aquí repeat se utiliza para hacer un bucle superior interactivo
%
interactive_loop:-
  repeat,
  write('Ingrese un comando y . (escriba end. para salir): '),
  read(X),
  write(X), nl,
  % usualmente haríamos algo de análisis aquí
  X = end.

