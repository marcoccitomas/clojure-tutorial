/*
     Programa para jugar al tres en raya
     Eres X y mueves primero.
     Para jugar, ingresa la ubicación en el tablero.

     El tablero se imprime de la siguiente manera:
     x2o
     4ox
     78x
*/

/* Un pequeño envoltorio para iniciar un juego de manera conveniente */
tictactoe :- game([b,b,b,b,b,b,b,b,b], x).

/* Llamada recursiva principal para jugar el juego */
game(Board, _) :- win(x, Board), write('¡Ganaste!').
game(Board, _) :- win(o, Board), write('¡Yo gano!').
game(Board, _) :- cat(Board).   /* No hay más movimientos - técnicamente no es gato */
game(Board, x) :- moveUser(Board, NewBoard), game(NewBoard, o).
game(Board, o) :- moveComputer(Board, NewBoard), game(NewBoard, x).

moveUser(Board, NewBoard) :- showBoard(Board), put('*'), get_char(Y), get_char(_), name(Y, [X]),
    Loc is X - 48, !, procUserMove(Loc, Board, NewBoard).

procUserMove(Loc, Board, NewBoard) :- Loc > 0, Loc < 10, playAt(x, Loc, Board, NewBoard).
/* Maneja el caso inválido */
procUserMove(_, Board, _) :- write('Movimiento inválido'), !, game(Board, x).

/* Listas deben tener la misma longitud - ¿cómo se activaron estas? */
/*playAt(_ , _ , [_|_] , []) :- !,fail.
playAt(_ , _ , [] , [_|_]) :- !,fail. */
playAt(Player, 1, [b|Tail], [Player|Tail]).
playAt(Player, Loc, [H|TBoard], [H|TNewBoard]) :-
   Loc > 1,
   M is Loc - 1,
   playAt(Player, M, TBoard, TNewBoard).

showBoard([]).
showBoard([H|T]) :- put(H), showBoard1(T).
showBoard1([H|T]) :- put(H), showBoard2(T).
showBoard2([H|T]) :- put(H), nl, showBoard(T).

win(A, [A,A,A,_,_,_,_,_,_]).
win(A, [_,_,_,A,A,A,_,_,_]).
win(A, [_,_,_,_,_,_,A,A,A]).

win(A, [A,_,_,A,_,_,A,_,_]).
win(A, [_,A,_,_,A,_,_,A,_]).
win(A, [_,_,A,_,_,A,_,_,A]).

win(A, [A,_,_,_,A,_,_,_,A]).
win(A, [_,_,A,_,A,_,A,_,_]).

cat([]).
cat([H|T]) :- H \= b, cat(T).

/* ¿Qué tablero ganador podemos hacer jugando en o más allá de Loc? */
playInstaWin(Loc, Board, NewBoard) :- Loc < 10, playAt(o, Loc, Board, NewBoard), win(o, NewBoard).
playInstaWin(Loc, Board, NewBoard) :- Loc < 9, M is Loc + 1, playInstaWin(M, Board, NewBoard).

/* N es el número de amenazas que el jugador Player tiene en este tablero */
countThreats(Player, Board, N) :- countThreatsFamulus(Player, Board, 0, N, [
[a,a,a,@,@,@,@,@,@],
[@,@,@,a,a,a,@,@,@],
[@,@,@,@,@,@,a,a,a],
[a,@,@,a,@,@,a,@,@],
[@,a,@,@,a,@,@,a,@],
[@,@,a,@,@,a,@,@,a],
[a,@,@,@,a,@,@,@,a],
[@,@,a,@,a,@,a,@,@]]).

countThreatsFamulus(_, _, N, N, []).
countThreatsFamulus(Player, Board, SoFar, N, [HThreat|TThreat]) :-
    threatp(Player, Board, HThreat),
    M is SoFar + 1,
    countThreatsFamulus(Player, Board, M, N, TThreat).
countThreatsFamulus(Player, Board, SoFar, N, [_|TThreat]) :-
    countThreatsFamulus(Player, Board, SoFar, N, TThreat).

/* ¿Está este jugador amenazando con ganar usando este patrón? */
/* Una amenaza es tener dos de los tres elementos requeridos llenos y el restante en blanco */
threatp(_, [], []) :- !, fail.
threatp(Player, [Player|TBoard], [a|TPattern]) :- direthreatp(Player, TBoard, TPattern).
threatp(Player, [b|TBoard], [a|TPattern]) :- threatp(Player, TBoard, TPattern).
threatp(Player, [_|TBoard], [@|TPattern]) :- threatp(Player, TBoard, TPattern).

direthreatp(_, [], []) :- !, fail.
direthreatp(Player, [Player|_], [a|_]).
direthreatp(Player, [b|TBoard], [a|TPattern]) :- direthreatp(Player, TBoard, TPattern).
direthreatp(Player, [_|TBoard], [@|TPattern]) :- direthreatp(Player, TBoard, TPattern).

/* Fin de las partes relacionadas con countThreats */

/* ¿Dónde se vería forzado o a jugar, si comenzamos a mirar en Loc y
Board es el trozo restante del tablero? */
playForcedMove(Loc, _, _) :- Loc > 9, !, fail.
playForcedMove(Loc, Board, NewBoard) :-
    playAt(o, Loc, Board, NewBoard),
    countThreats(x, Board, N),
    countThreats(x, NewBoard, M),
    M \= N.
playForcedMove(Loc, Board, NewBoard) :- Next is Loc + 1, playForcedMove(Next, Board, NewBoard).

/* Jugar donde podemos hacer dos o más amenazas */
playDualThreat(Loc, _, _) :- Loc > 9, !, fail.
playDualThreat(Loc, Board, NewBoard) :-
    playAt(o, Loc, Board, NewBoard),
    countThreats(o, NewBoard, NThreats),
    NThreats >= 2.
playDualThreat(Loc, Board, NewBoard) :-
    Next is Loc + 1,
    playDualThreat(Next, Board, NewBoard).

/* Jugar donde podemos hacer al menos una amenaza */
playThreat(Loc, _, _) :- Loc > 9, !, fail.
playThreat(Loc, Board, NewBoard) :-
    playAt(o, Loc, Board, NewBoard),
    countThreats(o, NewBoard, NThreats),
    NThreats >= 1.
playThreat(Loc, Board, NewBoard) :-
    Next is Loc + 1,
    playThreat(Next, Board, NewBoard).

/* Jugar en cualquier lugar donde podamos */
playRandom(Loc, _, _) :- Loc > 9, !, fail.
playRandom(Loc, Board, NewBoard) :-
    playAt(o, Loc, Board, NewBoard).
playRandom(Loc, Board, NewBoard) :-
    Next is Loc + 1,
    playRandom(Next, Board, NewBoard).

/* ¿Podemos poner estas en una lista - playInstaWin, etc? y DRY? */

/* Si podemos ganar en este turno, hazlo */
moveComputer(Board, NewBoard) :- playInstaWin(1, Board, NewBoard).

/* Si debemos jugar en algún lugar para evitar perder, hazlo */
moveComputer(Board, NewBoard) :- playForcedMove(1, Board, NewBoard).

/* De lo contrario, juega donde podemos hacer dos o más amenazas */
moveComputer(Board, NewBoard) :- playDualThreat(1, Board, NewBoard).

/* De lo contrario, juega donde podemos hacer al menos una amenaza */
moveComputer(Board, NewBoard) :- playThreat(1, Board, NewBoard).

/* De lo contrario, juega en cualquier lugar */
moveComputer(Board, NewBoard) :- playRandom(1, Board, NewBoard).

