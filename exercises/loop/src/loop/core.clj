(ns loop.core
  (:require [clojure.string :as str]))

;
; Recursión
;
(defn recurse
  ([]
   nil)
  ([[h & t]]
   (println h)
   (recur t)))

;
; Bucle impulsado por fallo
;
(defn fail-driven [lst]
  (doseq [x lst]
    (println x)))

;
; Map
;
(defn by-map [lst]
  (doall (map println lst)))

;
; Usar repeat para generar un número infinito de puntos de elección
; Aquí repeat se utiliza para hacer un bucle superior interactivo
;
(defn interactive-loop []
  (loop []
    (println "Ingrese un comando y . (escriba end. para salir): ")
    (let [x (read-line)]
      (println x)
      (if (= x "end.")
        (println "Saliendo...")
        (recur)))))


