(ns emoticons.core
  (:require [clojure.string :as str]))

;; Definición de significados de partes del emoticón
(def meanings
  {:hat {"" ""
         "d" "baseball cap worn sideways, or hat in general"
         "&" "brain (brainy, or fried), or hair"
         ")&$#|" "Carmen Miranda Hat"
         "<" "Dunce cap, pointed head, or thinking cap"
         ">" "Antenna"
         "=" "hair standing on end"
         "L" "combover"}

   :eyebrows {">" "inner eyebrows down, expressing worry or consternation"
              "|" "eyebrows corrugated, expressing sternness"}

   :eyes {"8" ""
          "B" "sunglasses"
          ":" "beady eyes"
          "X" "eyes closed or covered, in grief, disbelief, or pain"
          "o" "I'm a cyclops"
          "88" "wearing glasses"
          ";" "winking"}

   :nose {"c" ""
          "C" "big nose"
          "c:" "button nose"
          "2^o-uUOvV>" "non annie nose, reference to others"}

   :mouth {"*" "kissing"
           "+" "puckered mouth"
           "(" "sad"
           ")" "happy, smile"
           "E" "toothy (maybe gap tooth dumb)"
           "o" "o mouth, surprise or shock"
           "O" "mouth agape, shock"
           "[" "stern or painful grimace"
           "]" "trying not to laugh, or smile"
           "|" "closed mouth look"
           "S" "discomfort, pain"
           "d" "tongue hanging out (delicious, distaste, or concentration)"
           "D" "Very happy - or default"
           "F" "Vampire fangs"
           "7" "variant mouth"
           "X" "puckered mouth - pain, good grief, or ouch"
           "C" "sticking out lower lip"
           ">" "variant smile"
           "?" "puzzlement, empathy, or concern"
           "p" "tongue hanging out (delicious, distaste, or concentration)"
           "P" "tongue hanging out (delicious, distaste, or concentration)"
           "/\\" "irony"}})

;; Función auxiliar para obtener partes del emoticón
(defn part [emote symbol]
  (let [meanings-map (meanings emote)]
    (if-let [meaning (get meanings-map symbol)]
      meaning
      "")))

(defn emoticon-english [emote]
  (if (string? emote)
    (let [emote-codes (map str emote)] ; Convertir la cadena en una secuencia de caracteres
      (str/join " " (map #(part emote %) emote-codes))) ; Llamar a part con cada código de emoticón
    (str/join " " (map #(part emote %) emote)))) ; Llamar a part con cada código de emoticón


;; Ejemplo de uso
(defn -main []
  (println "Emoticón '8cD':")
  (println (emoticon-english "8cD"))
  (println)

  (println "Emoticón 'H|O[]':")
  (println (emoticon-english "H|O[]"))
  (println)

  (println "Emoticón '...':")
  (println (emoticon-english "..."))
  (println))

;; Ejecutar la función principal en el REPL
(-main)

