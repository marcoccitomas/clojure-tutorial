(ns tictactoe.core
  (:gen-class))

;; Definición de datos para representar el tablero y jugadores
(def x-player "x")
(def o-player "o")
(def blank " ")

;; Función principal para iniciar el juego
(defn -main []
  (println "Bienvenido al juego de Tres en Raya.")
  (tictactoe))

;; Función para iniciar un nuevo juego
(defn tictactoe []
  (let [board (vec (repeat 9 blank))]
    (loop [current-player x-player
           current-board board]
      (show-board current-board)
      (if (winning-line? current-player current-board)
        (println (str "¡" current-player " gana!"))
        (if (cat? current-board)
          (println "¡Empate!")
          (do
            (if (= current-player x-player)
              (recur o-player (make-move current-player current-board))
              (recur x-player (make-move current-player current-board)))))))))

;; Función para mostrar el tablero
(defn show-board [board]
  (doseq [row (partition 3 board)]
    (println (apply str row))))

;; Función para verificar si hay una línea ganadora
(defn winning-line? [player board]
  (let [lines [[0 1 2] [3 4 5] [6 7 8]
               [0 3 6] [1 4 7] [2 5 8]
               [0 4 8] [2 4 6]]]
    (some #(every? (partial = player) (map board %)) lines)))

;; Función para verificar si el tablero está lleno (empate)
(defn cat? [board]
  (not-any? #(= blank %) board))

;; Función para hacer un movimiento válido
(defn valid-move? [move board]
  (= blank (nth board (dec move))))

;; Función para realizar un movimiento
(defn make-move [player board]
  (println (str "Turno de " player))
  (println "Ingrese el número de casilla (1-9): ")
  (let [move (read-line)
        move-int (Integer/parseInt move)]
    (if (valid-move? move-int board)
      (assoc board (dec move-int) player)
      (do
        (println "Movimiento inválido, intenta de nuevo.")
        (make-move player board)))))

;; Ejecutar el juego al iniciar el archivo
(-main)

