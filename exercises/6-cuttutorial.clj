(ns park.core
  (:gen-class))

;; Definición de ubicaciones
(def locations #{:city_hall :vacant_lot :old_mill :field :barber_shop})

;; Predicados para propiedades de las ubicaciones
(defn flat? [loc]
  (case loc
    :city_hall true
    :vacant_lot true
    :field true
    :else false))

(defn grassy? [loc]
  (case loc
    :city_hall true
    :vacant_lot true
    :else false))

;; Predicado para posibles parques
(defn possibly-park? [loc]
  (and (locations loc)
       (flat? loc)
       (grassy? loc)))

;; Definición del parque definitivo
(def definitely-park :littletown_park)

;; Función para determinar el parque con lógica similar a Prolog
(defn park [loc]
  (cond
    (= loc definitely-park) ; Si la ubicación es el parque definitivo
    true
    :else                   ; De lo contrario, considerar todas las ubicaciones posibles
    (possibly-park? loc)))

;; Ejemplo de uso
(defn -main []
  (println "Posibles parques:")
  (doseq [loc locations]
    (when (park loc)
      (println loc))))

;; Ejecutar la función principal en el REPL
(-main)

