(ns sudoku.core
  (:require [clojure.string :as str]))

(defn all-different
  "Constraint to ensure all elements in a list are different."
  [vars]
  (apply distinct? vars))

(defn solve-sudoku [puzzle blocks]
  (if (every? #(every? number? %) puzzle)
    [puzzle]
    (let [[x y] (first (first (sort-by #(count (second %)) (for [i (range 9)
                                                                 j (range 9)
                                                                 :when (nil? (nth puzzle i j))] [i j blocks]))))
          valid-numbers (filter #(and (every? (fn [[v c]] (or (= x v) (= y c) (= (mod x 3) (mod v 3)) (= (mod y 3) (mod c 3)))) (blocks x y) (every? number? %)) (map list (repeat 10) (repeatedly 10 [x y]))))]
      (loop [i 1]
        (if (= i 10)
          nil
          (if (and (every? (fn [v] (or (not (= v i)) (= v 0))) (puzzle x))
                   (every? (fn [[v c]] (or (not (= i v)) (not (= i c)))) (blocks x y))
                   (every? number? (map list (repeat x) puzzle))
                   (every? number? (map list puzzle (repeat y))))
            (if-let [solution (solve-sudoku (assoc puzzle x (assoc (puzzle x) y i)) blocks)]
              solution
              (recur (inc i)))
            (recur (inc i)))))))

(defn sudoku
  "Solves the Sudoku puzzle using backtracking."
  [puzzle]
  (let [n 9
        rows (partition n puzzle)
        cols (apply map vector rows)
        squares (for [i (range 0 n 3)
                      j (range 0 n 3)]
                  (for [x (range 3)
                        y (range 3)]
                    (nth puzzle (+ (* i n) (* 3 x) j y))))
        blocks (concat rows cols squares)]
    (solve-sudoku puzzle blocks)))

(defn print-sudoku
  "Prints the Sudoku puzzle."
  [puzzle]
  (doseq [row puzzle]
    (println (apply str/join " " (map #(or % "_") row)))))


(defn -main
  "Entry point for the application."
  [& args]
  (let [puzzle [[5 3 0 0 7 0 0 0 0]
                [6 0 0 1 9 5 0 0 0]
                [0 9 8 0 0 0 0 6 0]
                [8 0 0 0 6 0 0 0 3]
                [4 0 0 8 0 3 0 0 1]
                [7 0 0 0 2 0 0 0 6]
                [0 6 0 0 0 0 2 8 0]
                [0 0 0 4 1 9 0 0 5]
                [0 0 0 0 8 0 0 7 9]]]
    (println "Sudoku puzzle:")
    (print-sudoku puzzle)
    (println "\nSolving...")
    (let [solution (sudoku puzzle)]
      (if solution
        (do
          (println "Solution:")
          (print-sudoku solution))
        (println "No solution found."))))))
