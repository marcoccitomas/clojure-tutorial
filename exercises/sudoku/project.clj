(defproject sudoku "1.0.0"
  :description "Sudoku solver"
  :dependencies [[org.clojure/clojure "1.11.3"]
                 ;; Agrega otras dependencias si las necesitas
                 ]
  :main sudoku.core
  ) ;; Esto indica que el punto de entrada principal es en el namespace sudoku.core

