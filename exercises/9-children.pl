%   Un maestro desea sentar a 16 niños conflictivos
%   en una disposición de sillas de 4x4.
%   Algunos de los niños no se llevan bien.
%   El problema es sentar a los niños de manera que ningún estudiante
%   esté sentado adyacente (en las 4 direcciones) a un niño
%   con el que se pelee.
%
%  Los niños están numerados del 1 al 16.
%  Del 1 al 8 son niñas, del 9 al 16 son niños.
%
%
:- use_module(library(clpfd)).

compatible(Vecino, Estudiante) :-
    Estudiante #= 3   #==> Vecino #< 9, % A la estudiante #3 no le gusta sentarse junto a chicos
    Estudiante #= 5 #==> Vecino #< 9, % La estudiante #5 está de acuerdo con la #3
    Estudiante #= 2 #==> Vecino #\= 3, % Los estudiantes #2 y #3 no se llevan bien
    Estudiante #= 10  #==> Vecino #> 8. % Al estudiante #10 no le gustan las chicas

constrain_up(1, _, _, _).
constrain_up(R, C, Estudiante, Board) :-
    R > 1,
    NR is R - 1,
    member(seat(NR, C, Vecino), Board),
    compatible(Vecino, Estudiante).

constrain_down(4, _, _, _).
constrain_down(R, C, Estudiante, Board) :-
    R < 4,
    NR is R + 1,
    member(seat(NR, C, Vecino), Board),
    compatible(Vecino, Estudiante).

constrain_left(_, 1, _, _).
constrain_left(R, C, Estudiante, Board) :-
    C > 1,
    NC is C - 1,
    member(seat(R, NC, Vecino), Board),
    compatible(Vecino, Estudiante).

constrain_right(_, 4, _, _).
constrain_right(R, C, Estudiante, Board) :-
    C < 4,
    NC is C + 1,
    member(seat(R, NC, Vecino), Board),
    compatible(Vecino, Estudiante).

constrain_pupil(Board, seat(R, C, Estudiante)) :-
    constrain_up(R, C, Estudiante, Board),
    constrain_down(R, C, Estudiante, Board),
    constrain_left(R, C, Estudiante, Board),
    constrain_right(R, C, Estudiante, Board).

make_seat(R, C, seat(R, C, Estudiante)) :-
    Estudiante in 1..16.

% Mapear entre seat(r,c,s) y variable cruda
seat_student(seat(_, _, S), S).

% Mapear entre la representación [seat(1,1,S1)...] y la representación [S1]
board_students(In, _SoFar, Raw) :-
    maplist(seat_student, In, Raw).

/*
    Cómo mapear manualmente  8cD

board_students([], _, []).
board_students([seat(_, _, S)|T], _, [S|Vs]) :-
    board_students(T, _, Vs).
*/

make_board(Board) :-
    findall(S,
            (   member(R, [1,2,3,4]),
                member(C, [1,2,3,4]),
                make_seat(R, C, S)),
            Board),
    maplist(seat_student, Board, Raw),
    all_distinct(Raw).

write_board(Board) :-
    member(R, [1,2,3,4]),
    nl,
    member(C, [1,2,3,4]),
    member(seat(R, C, S), Board),
    write(S), write(' '),
    fail.
write_board(_) :- nl.

assign_all_pupils :-
    make_board(Board),
    maplist(constrain_pupil(Board), Board),
    maplist(seat_student, Board, Raw),
    labeling([], Raw),
    write_board(Board).

