/* Este ejemplo resuelve preguntas sobre "árboles tecnológicos"
 * utilizando Constraint Handling Rules (Reglas de Manejo de Restricciones).
 *
 * Los árboles tecnológicos son un mecanismo común en los juegos donde
 * el jugador, u oponente controlado por inteligencia artificial, debe
 * construir un edificio o unidad específica antes de poder construir otro.
 * Al ver una unidad, el jugador oponente puede inferir que su oponente
 * posee todas las unidades necesarias para ella.
 * Por ejemplo, si construir tanques requiere una fábrica de tanques,
 * y la fábrica de tanques requiere una fundición, entonces si vemos un tanque
 * podemos inferir que tienen la fundición.
 *
 * Nuestra lógica no tiene en cuenta la destrucción posterior de unidades.
 *
 * El uso se complica ligeramente por el hecho de que volver al nivel superior
 * limpia el almacén de restricciones. Por lo tanto, se proporciona el predicado
 * de conveniencia `i_saw`.
 *
 * Sintaxis CHR, ya que nunca puedo recordarla:
 * name @ retained \ discarded <=> guard | head, body.    Simpagation
 * name @ discarded <=> guard | head, body.      Simplification
 * name @ retained ==> guard | head, body.         Propagation
 *
 * Uso:
 * ?- init, i_saw(boat).
 * ?- init, i_saw([boat, knight]).
 *
 * Es útil usar el depurador CHR.
 *
 * ?- chr_trace, init, i_saw(boat).
 *
 * Referencias útiles:
 * https://dtai.cs.kuleuven.be/CHR/files/tutorial_iclp2008.pdf
 * https://dtai.cs.kuleuven.be/CHR/
 * https://dtai.cs.kuleuven.be/CHR/webchr.shtml
 * https://www.swi-prolog.org/pldoc/man?section=chr
 *
 * Derechos de autor (c) 2019   Anne Ogborn
 * Liberado bajo la licencia MIT adjunta
 */

:- use_module(library(chr)).

% Las restricciones CHR deben definirse utilizando esta directiva
:- chr_constraint
    saw/1,         % Vi una unidad de este tipo
    has/1,         % Puedo inferir que el enemigo tiene una unidad de este tipo
    can_build/1,   % Puedo inferir que el enemigo puede construir una unidad de este tipo
    can_build_if/2, % El enemigo puede construir una unidad de este tipo si el arg2 lista todas existen
    needs/2,       % Regla del juego, para construir arg1, todas las de la lista arg2 deben existir
    reset/0.       % Restablecer el mundo del juego

% Restablecer los elementos del juego
reset \ saw(_) <=> true.
reset \ has(_) <=> true.
reset \ can_build(_) <=> true.
reset \ needs(_, _) <=>true.
reset \ can_build_if(_, _) <=> true.
reset <=> true.

%
% Establecer el estado inicial del almacén de restricciones,
% con las dependencias del juego y dos campesinos iniciales
%
% Para hacer barracas se necesita campesino, etc.
init :-
    needs(barracks, [peasant]),
    needs(stable, [peasant, barracks]),
    needs(dock, [peasant, barracks]),
    needs(swordsman, [barracks]),
    needs(knight, [stable]),
    needs(boat, [dock, peasant]),
    saw(peasant),  % 'Vimos' el campesino
    saw(peasant).  % porque el juego comienza con 2 campesinos

% Imponer semántica de conjunto para varias cosas
% una vez que sabemos que tienen un barco, no queremos agregarlo nuevamente
saw(X), saw(X) <=> saw(X).
has(X), has(X) <=> has(X).
can_build(X), can_build(X) <=> can_build(X).
can_build_if(X, Y), can_build_if(X, Y) <=> can_build_if(X, Y).

% Sentido común
saw(X) ==> has(X).         % Lo vi, ellos lo tienen
has(X) ==> can_build(X).   % Lo tienen, pueden construirlo

% Esto solo existe brevemente
:- chr_constraint must_have/1.

% Expresa la idea 'tienen tanques, deben tener una fábrica de tanques'
% porque needs tiene una lista, dispararemos recursivamente la regla must_have
% para agregar todo
has(X), needs(X, List) ==> must_have(List).
must_have([]) <=> true.
must_have([X|Y]) <=> has(X), must_have(Y).

% Agregar can_build para todo cuyas necesidades existan
% tener muelle y campesino agrega can_build(boat).
% esperamos hasta que exista el primer elemento de la lista,
% luego pasamos al segundo elemento y esperamos, y así sucesivamente
needs(X, Z) ==> can_build_if(X, Z).
can_build_if(X, []) <=> can_build(X).
has(Y), can_build_if(X, [Y | Z]) <=> can_build_if(X, Z), has(Y).

% Predicado Prolog de conveniencia para pruebas.
% pasar lista de unidades vistas
i_saw(X) :-
    atomic(X),
    call(saw(X)),
    print_store.
i_saw(X) :-
    is_list(X),
    maplist(callsaw , X),
    print_store.

callsaw(X) :- call(saw(X)).

%
% Imprimir los resultados del cálculo, mostrando
% lo que el enemigo ha construido y lo que pueden construir
%
print_store :-
    writeln('==============='),
    find_chr_constraint(has(Y)),
    format('Tu enemigo ha construido ~w~n', [Y]),
    fail.
print_store :-
    nl,
    find_chr_constraint(can_build(Y)),
    format('Tu enemigo puede construir ~w~n', [Y]),
    fail.
print_store.

		 /*******************************
		 *     utilidades útiles        *
		 *******************************/

% Imprimir el almacén de restricciones
ps :-
    find_chr_constraint(Y),
    format('El almacén de restricciones contiene ~w~n', [Y]),
    fail.
ps.

% Imprimir el almacén de restricciones al volver al nivel superior
ss :- set_prolog_flag(chr_toplevel_show_store, true).

% o no
noss :- set_prolog_flag(chr_toplevel_show_store, false).

