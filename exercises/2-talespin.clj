(ns talespin.core
  (:require [clojure.set :refer [subset?]]
            [clojure.string :as str]))

;; Definición de situaciones iniciales y objetivos
(def initial-situation
  #{[:plocation :passengers1 :gate :seattle]
    [:alocation :airplane1 :gate :seattle]
    [:flight-path :seattle :chicago]
    [:flight-path :chicago :dallas]
    [:airplane :airplane1]
    [:passengers :passengers1]})

(def initial-goal
  [:plocation :passengers1 :gate :dallas])

;; Base de conocimientos de eventos y definiciones
(def event-db
  {:action {:load {:pc [:plocation :passengers :gate :airport]
                   :achieve [:contains :airplane :passengers]
                   :txt "The passengers boarded the plane."}
            ;; Otras definiciones de acciones...
            }
   :happening {:fire {:achieve [:on-fire :engine]
                       :txt "The engine caught fire."}
               ;; Otras definiciones de sucesos...
               }})

;; Función principal para ejecutar el programa de talespin
(defn talespin []
  (let [initial-plan (make-best-plan initial-goal initial-situation)
        prob 0.3
        [story-actions _story-situations] (execute-plan initial-plan initial-situation initial-goal prob)]
    (str "Once upon a time...\n"
         (str/join "\n" (anglify story-actions)))))

;; Función para encontrar el mejor plan
(defn make-best-plan [goal situation]
  (let [ranked-plans (sort-by #(plan-quality %) (mapcat #(make-plan goal situation %) situation))]
    (first ranked-plans)))

;; Función para calcular la calidad de un plan
(defn plan-quality [plan]
  (let [length (count plan)
        cost (if (some #(= :evacuate %) plan) 1 0)]
    (- 100 (* length 10) cost)))

;; Función para generar un plan
(defn make-plan [goal situation current-plan]
  (cond
    (satisfied goal situation) [current-plan situation []]
    :else
    (let [[action new-situation actions] (first (for [event (event-definitions :action)
                                                      :when (achieves event goal)]
                                                  (let [[new-situation actions] (make-plans (:pc event) situation current-plan)]
                                                    [event new-situation actions])))]
      [current-plan new-situation actions])))

;; Función para generar planes
(defn make-plans [pc-list situation current-plan]
  (reduce (fn [[situation actions] pc]
            (let [[new-situation new-actions] (make-plan (:achieve pc) situation current-plan)]
              [new-situation (concat actions new-actions)]))
          [situation []]
          pc-list))

;; Función para ejecutar un plan
(defn execute-plan [actions situation goal prob]
  (loop [actions actions
         situation situation
         goal goal
         story-actions []
         story-situations []]
    (if (empty? actions)
      [story-actions story-situations]
      (let [[new-action new-situation] (if (maybe prob)
                                         (do-event (rand-nth (filter #(satisfieds % situation) (event-definitions :happening))) situation)
                                         (do-event (first actions) situation))]
        (recur (rest actions)
               new-situation
               (revise-goal new-situation goal)
               (if (maybe prob)
                 (conj story-actions new-action)
                 story-actions)
               (conj story-situations new-situation))))))

;; Función para realizar un evento
(defn do-event [event situation]
  (let [[_ _ dels adds] (event-definitions :event event)]
    (apply-effects dels adds situation)))

;; Función para aplicar efectos
(defn apply-effects [dels adds situation]
  (let [new-situation (apply dissoc situation dels)]
    (apply merge new-situation adds)))

;; Función para revisar el objetivo
(defn revise-goal [situation goal]
  (cond
    (contains? situation [:on-fire :engine]) [:a-on-ground :airplane]
    (contains? situation [:ill-passenger]) [:medical-help :passengers]
    :else goal))

;; Función para verificar si un objetivo está satisfecho
(defn satisfied [goal situation]
  (contains? situation goal))

;; Función para verificar si un conjunto de metas está satisfecho
(defn satisfieds [goals situation]
  (every? #(satisfied % situation) goals))

;; Función para seleccionar un elemento aleatorio de una lista
(defn rand-nth [coll]
  (let [idx (rand-int (count coll))]
    (nth coll idx)))

;; Función para evaluar si ocurre un evento con cierta probabilidad
(defn maybe [prob]
  (< (rand) prob))

;; Función para acceder a definiciones de eventos
(defn event-definitions [type & args]
  (if args
    (get-in event-db [type (first args)])
    (get event-db type)))

;; Función para imprimir una lista de acciones en formato narrativo
(defn anglify [actions]
  (map #(get-in (event-definitions :action %) [:txt]) actions))

;; Inicia el programa de talespin
(defn -main []
  (let [story (talespin)]
    (println story)))

