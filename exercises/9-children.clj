(ns constraints.core
  (:require [clojure.core.logic :refer [run* fresh == conde membero lvar lvaro]]
            [clojure.core.logic.fd :as fd]
            [clojure.set :refer [difference]]))

;;
;; Un maestro desea sentar a 16 niños conflictivos
;; en una disposición de sillas de 4x4.
;; Algunos de los niños no se llevan bien.
;; El problema es sentar a los niños de manera que ningún estudiante
;; esté sentado adyacente (en las 4 direcciones) a un niño
;; con el que se pelee.
;;
;; Los niños están numerados del 1 al 16.
;; Del 1 al 8 son niñas, del 9 al 16 son niños.
;;

(defn compatible [neighbor student]
  (conde
    [(== student 3) (fd/< neighbor 9)] ; #3 no quiere sentarse junto a chicos
    [(== student 5) (fd/< neighbor 9)] ; #5 está de acuerdo con #3
    [(== student 2) (fd/!= neighbor 3)] ; #2 y #3 no se llevan bien
    [(== student 10) (fd/> neighbor 8)])) ; #10 no le gustan las chicas

(defn constrain-up [r c student board]
  (conde
    [(== r 1)]
    [(fd/> r 1)
     (fresh [nr neighbor]
       (== nr (- r 1))
       (membero [nr c neighbor] board)
       (compatible neighbor student))]))

(defn constrain-down [r c student board]
  (conde
    [(== r 4)]
    [(fd/< r 4)
     (fresh [nr neighbor]
       (== nr (+ r 1))
       (membero [nr c neighbor] board)
       (compatible neighbor student))]))

(defn constrain-left [r c student board]
  (conde
    [(== c 1)]
    [(fd/> c 1)
     (fresh [nc neighbor]
       (== nc (- c 1))
       (membero [r nc neighbor] board)
       (compatible neighbor student))]))

(defn constrain-right [r c student board]
  (conde
    [(== c 4)]
    [(fd/< c 4)
     (fresh [nc neighbor]
       (== nc (+ c 1))
       (membero [r nc neighbor] board)
       (compatible neighbor student))]))

(defn constrain-pupil [board [r c student]]
  (all
    (constrain-up r c student board)
    (constrain-down r c student board)
    (constrain-left r c student board)
    (constrain-right r c student board)))

(defn make-seat [r c]
  (fresh [student]
    (fd/in student (fd/interval 1 16))
    [r c student]))

(defn seat-student [[_ _ s]] s)

(defn make-board []
  (let [positions (for [r (range 1 5)
                        c (range 1 5)]
                    (make-seat r c))]
    (map seat-student positions)))

(defn write-board [board]
  (doseq [r (range 1 5)]
    (println)
    (doseq [c (range 1 5)]
      (let [student (some #(when (and (= (first %) r) (= (second %) c)) (nth % 2)) board)]
        (print (str student " ")))))
  (println))

(defn assign-all-pupils []
  (let [board (make-board)]
    (run* [q]
      (fresh [raw]
        (all
          (== raw board)
          (fd/distinct raw)
          (map (partial constrain-pupil board) board)
          (fd/label raw))
        (write-board raw)))))

;; Para ejecutar:
(assign-all-pupils)

