(ns cannibals.core)

;; Verifica si el estado es válido: no más caníbales que misioneros en ninguna orilla
(defn valid-state? [lc lm rc rm]
  (and (>= lc 0) (>= lm 0) (>= rc 0) (>= rm 0)
       (or (<= lc lm) (= lm 0))
       (or (<= rc rm) (= rm 0))))

;; Calcula el nuevo estado después de un movimiento
(defn move [lc lm rc rm dc dm direction]
  (let [nlc (+ lc (* direction dc))
        nlm (+ lm (* direction dm))
        nrc (- rc (* direction dc))
        nrm (- rm (* direction dm))]
    [nlc nlm nrc nrm]))

;; Resuelve el problema de los caníbales y misioneros
(defn cannibals [lc lm rc rm direction visited path]
  (if (and (= lc 0) (= lm 0))
    [path]
    (apply concat
           (for [[dc dm] [[1 0] [0 1] [1 1] [2 0] [0 2]]
                 :let [[nlc nlm nrc nrm] (move lc lm rc rm dc dm direction)
                       new-direction (* -1 direction)
                       state [nlc nlm nrc nrm]]
                 :when (and (valid-state? nlc nlm nrc nrm)
                            (not (visited state)))]
             (cannibals nlc nlm nrc nrm new-direction (conj visited state)
                        (conj path [direction dc dm]))))))

;; Encuentra la solución inicial
(defn solve []
  (first (remove nil? (cannibals 3 3 0 0 -1 #{[3 3 0 0]} []))))

;; Imprime la solución paso a paso
(defn show-solution-helper [lc lm rc rm path]
  (when (seq path)
    (let [[direction dc dm] (first path)
          [nlc nlm nrc nrm] (move lc lm rc rm dc dm direction)]
      (if (= direction -1)
        (println (format "%d,%d   \\_______/   %d,%d\n    <-(%d,%d)--"
                         lc lm rc rm dc dm))
        (println (format "%d,%d   \\_______/   %d,%d\n       --(%d,%d)->"
                         lc lm rc rm dc dm)))
      (recur nlc nlm nrc nrm (rest path)))))

(defn show-solution [lc lm rc rm path]
  (show-solution-helper lc lm rc rm path))

;; Función principal
(defn -main []
  (let [solution (solve)]
    (if solution
      (show-solution 3 3 0 0 solution)
      (println "No hay solución encontrada"))))

;; Ejecutar función principal en el REPL
(-main)

