(ns nqueen.core
  (:require [clojure.math.combinatorics :as comb]))

(defn queens [n]
  (let [positions (range 1 (inc n))]
    (comb/permutations positions)))

(defn diagonal? [q1 q2 dist]
  (or (= q1 q2)
      (= (Math/abs (- q1 q2)) dist)))

(defn safe-placement? [queens]
  (let [pairs (comb/combinations (map-indexed vector queens) 2)]
    (not-any? (fn [[[q1 i1] [q2 i2]]]
                (diagonal? q1 q2 (Math/abs (- i1 i2))))
              pairs)))


(defn find-safe-placements [n]
  (let [all-permutations (queens n)]
    (filter safe-placement? all-permutations)))

(defn -main [& args]
  (let [n (Integer/parseInt (first args))]
    (when (pos? n)
      (let [solutions (find-safe-placements n)]
        (if (empty? solutions)
          (println "No solutions found.")
          (doseq [solution solutions]
            (println (str "Solution: " solution))))))))

