:- use_module(library(clpfd)).
%
% Demostración simple que restringe una lista a ser la suma por pares de otras
% dos listas
%

addlists([], [], []).

addlists([HA|TA],[HB|TB],[HC|TC]):-
   HC #= HA+HB,
   addlists(TA,TB,TC).

