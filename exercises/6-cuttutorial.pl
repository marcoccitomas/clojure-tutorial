%
% Esto es para ayudar a los programadores principiantes de Prolog a entender los cortes
% mediante una pequeña historia
%
% Esta no es toda la historia de los cortes. Solo intenta ayudarte a 'comprender'
% lo que hace 'cut' y lo que hace 'cut, fail'.

%
% Eres un escritor de no ficción investigando un evento que ocurrió en un
% parque en el pequeño pueblo de Littletown hace varias décadas: la
% invención de la tostadora voladora.
%
% Tienes la oportunidad de conducir hasta Littletown para un par de días de
% investigación.
% Ahora quieres encontrar 'el parque'. Los relatos existentes son bastante
% vagos sobre dónde está.
% Conduciendo por ahí no ves nada que sea obviamente 'el parque',
% pero encuentras varios lugares que podrían ser 'el parque' - un solar vacío,
% los amplios céspedes alrededor del ayuntamiento, etc.
%
% Vamos a modelar el proceso de encontrar 'el parque'.

% Algunas ubicaciones
% La primera cláusula significa que city_hall es una ubicación.
location(city_hall).
location(vacant_lot).
location(old_mill).
location(field).
location(barber_shop).

%
% Lugares que son planos y con césped podrían ser 'el parque'.

flat(city_hall).
flat(vacant_lot).
flat(field).

grassy(city_hall).
grassy(vacant_lot).


%
% Lugares que podrían ser el parque
possibly_park(X) :-
	location(X),
	flat(X),
	grassy(X).

%
% Ahora puedes consultar dónde podría estar el parque de la siguiente manera:
% 	?- possibly_park(X).
% y obtendrás algunas ubicaciones posibles.

%
% Ahora supón que doblas una esquina y encuentras un parque,
% con juegos de columpios, mesas de picnic, parrillas,
% y un letrero grande que dice 'Parque Littletown. En este
% lugar se inventó la tostadora voladora en 1987 por
% Un Programador Aleatorio.
%
% Después de encontrar esto, es obvio que 'el parque' no es
% el solar vacío o el ayuntamiento.
% Vamos a hacer una nueva consulta que maneje esto.
%
% Definiremos algún lugar como definitivamente el parque.
definitely_park(littletown_park).

% y haremos una consulta que encuentre un conjunto de respuestas óptimas
% para la ubicación del parque
%
% esta cláusula dice 'si hemos encontrado definitivamente el parque,
% ignorar los posibles_park's  -
park(X) :-
	definitely_park(X), % este es el parque
	!. % ignorar todas las demás soluciones a park

%
% esta cláusula dice qué sucede de lo contrario
park(X) :-
	possibly_park(X).

%
% Ejecute esta consulta:
% 	?- park(X).
% y verá que hay solo una respuesta. Comente el hecho definitely_park
% y verá todas las posibles.

%
% Genial - entonces, cambiemos la historia de nuevo.
% En lugar de encontrar (lo que es definitivamente) el parque,
% te cansaste de conducir por ahí y te detuviste en una tienda
% para pedir direcciones al parque.
% El tipo en la tienda dice que usaron el terreno del parque para
% construir un nuevo auditorio municipal en 1995.
%
% Así que construyamos un park2 que maneje esta situación.

park_gone.


park2(_) :-         % _ porque no nos importará qué ubicación se pasa
	park_gone,  % si el parque se ha ido
	!,          % ignorar otras soluciones
	fail.       % pero esto tampoco es una solución

% lo que queda como parque
park2(X) :-
	definitely_park(X),
	!.

park2(X) :-
	possibly_park(X).

% Ejecute la consulta:
% 	?- park2(X).
%
% y obtendrá falso. Comente park_gone, y obtendrá littletown_park.
% Si comenta definitely_park, obtendrá todas las posibles.
%

