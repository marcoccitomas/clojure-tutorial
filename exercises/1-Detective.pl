%
%  Dado el siguiente problema: 
%
%  Eres un detective tratando de resolver un caso de asesinato
%  Hay tres sospechosos - Art, Burt y Carl
%  También son los únicos tres testigos
%
%  Aquí están sus declaraciones:
%  Art:
%  Burt era amigo de la víctima, pero la víctima y Carl eran enemigos mortales.
%
%  Burt:
%  Estaba fuera de la ciudad cuando sucedió, y además ni siquiera conocía al tipo.
%
%  Carl:
%  Soy inocente. No sé quién lo hizo. Vi a Art y Burt conduciendo
%  por la ciudad entonces.
%
%  Determine quién está mintiendo.
%
% M es el asesino
%  a, b y c son Art, Burt y Carl
%  W es la lista actual de testigos
%

%  Art:
%  Burt era amigo de la víctima, pero la víctima y Carl eran enemigos mortales.

testimony(a, friend(b)).  % según Art, Burt era amigo de la víctima
testimony(a, enemy(c)).


%  Burt:
%  Estaba fuera de la ciudad cuando sucedió, y además ni siquiera conocía al tipo.
testimony(b, out_of_town(b)).
testimony(b, stranger(b)).

%  Carl:
%  Soy inocente. No sé quién lo hizo. Vi a Art y Burt conduciendo
%  por la ciudad entonces.
%
testimony(c, in_town(c)). % Carl estaba en la ciudad si vio a Art y Burt
testimony(c, in_town(a)).
testimony(c, in_town(b)).

%
% Ahora defino qué significa inconsistente
inconsistent(friend(X), enemy(X)).
inconsistent(friend(X), stranger(X)).
inconsistent(enemy(X), stranger(X)).
inconsistent(out_of_town(X), in_town(X)).


%%	asesino(?Sospechoso:átomo) is nondet
%
% llamada principal
%
% Sospechoso es el sospechoso
%
% @arg Sospechoso el criminal que cometió el crimen
%

asesino(M) :-
	member(M, [a, b, c]), % escoger a alguien
	select(M, [a, b, c], Testigos), % ignorar su testimonio
	consistent(Testigos).   % si su historia es consistente, M es sospechoso

%
% Un conjunto de testigos es consistente si entre ellos no hay
% testimonios inconsistentes
%
consistent(W) :-
	\+ testimonio_inconsistente(W).

%
% Un conjunto de testigos tiene testimonios inconsistentes
% si dos piezas de testimonio pertenecientes a dos testigos diferentes
% son inconsistentes
%
testimonio_inconsistente(W) :-
	member(X, W),  % X e Y son testigos
	member(Y, W),
	X \= Y,	       % y son diferentes
	testimony(X, XT), % una pieza del testimonio de X
	testimony(Y, YT), % una pieza del testimonio de Y
	inconsistent(XT, YT).  % que son inconsistentes

