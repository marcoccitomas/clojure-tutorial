:- module(talespin, [talespin/0]).

% Archivo: talespin.pl
% Autor: Peter Clark
% Fecha: Enero 1999
% Propósito: Reconstrucción simple y altamente improvisada del generador de historias
%            TALE-SPIN de Meehan, aquí aplicado a "historias" de incidentes de aviación.
%            Esta reconstrucción sin duda omite muchas partes del programa original de Meehan,
%            y también añade nuevas partes/enfoques que Meehan no utilizó originalmente.

talespin :-
        InitialSituation =
                    [ plocation(passengers1, gate(seattle)),
                      alocation(airplane1, gate(seattle)),
                      flight_path(seattle, chicago),
                      flight_path(chicago, dallas),
                      airplane(airplane1),
                      passengers(passengers1) ],
        InitialGoal = plocation(passengers1, gate(dallas)),
        make_best_plan(InitialGoal, InitialSituation, InitialPlan),
        Prob = 0.3,             % Probability of incident occurring at a particular step
        execute_plan(InitialPlan, InitialSituation, InitialGoal,
                                Prob, StoryActions, _StorySituations),
        write('Once upon a time...'), nl,
        anglify(StoryActions, StoryText),
        lwrite(StoryText).

% ======================================================================
%               EL PLANIFICADOR SIMILIAR A STRIPS
% ======================================================================
% make_plan/3: Planificador simple de encadenamiento hacia atrás, sin una lista de objetivos protegida.
make_best_plan(Goal, Situation, BestPlan) :-
        findall(Quality-Plan,
                   ( make_plan(Goal,Situation,Plan),
                     plan_quality(Plan,Quality) ),
                RankedPlans),
        sort(RankedPlans, OrderedPlans),
        last(OrderedPlans, _-BestPlan).

plan_quality(Plan, Quality) :-
        length(Plan, Length),                                 % pierde 10 puntos por paso
        ( member(evacuate(_),Plan) -> Cost = 1 ; Cost = 0 ),  % pierde 1 por evacuar
        Quality is 100 - Length*10 - Cost.

% ----------

make_plan(Goal, Situation, Plan) :-
        make_plan(Goal, Situation, [], _FinalSituation, Plan).

make_plan(Goal, Situation, _, Situation, []) :-
        satisfied(Goal, Situation).                    
make_plan(Goal, Situation, GoalStack, NewSituation, Actions) :-
        \+ satisfied(Goal, Situation),
        \+ member(Goal, GoalStack),                           
        event_definition(action, Action, PCs, Dels, Adds),
        achieves(PCs, Dels, Adds, Goal),
        make_plans(PCs, Situation, [Goal|GoalStack], MidSituation, PreActions),
        apply_effects(Dels, Adds, MidSituation, NewSituation),
        append(PreActions, [Action], Actions).

make_plans([], Situation, _, Situation, []).
make_plans([Goal|Goals], Situation, GoalStack, NewSituation, Actions) :-
        make_plan(Goal, Situation, GoalStack, MidSituation, FirstActions),
        make_plans(Goals, MidSituation, GoalStack, NewSituation, RestActions),
        append(FirstActions, RestActions, Actions).

achieves(_, _, Adds, Goal) :-                           
        member(Goal, Adds).
achieves(_, _, Adds, Goal) :-                           
        rule(( Goal :- Facts )),
        subset(Facts, Adds).

% ======================================================================
%                       EL SIMULADOR
% Esto es similar al planificador, *excepto* que también saboteará el trabajo (es decir, un suceso),
% requiriendo replanificación.
% ======================================================================
% Clausula final - hemos terminado
execute_plan([], FinalSituation, _, _, [], [FinalSituation]) :-
        !.

% 'happening' (incidente) ocurre
execute_plan(_Actions, Sitn, Goal, P, [Happening|NextActions], [Sitn|NextSitns]) :-
        maybe(P),                  % ¡Ocurre un incidente! Abandonar acciones y objetivos antiguos...
        !,
        findall(Happening,
                   ( event_definition(happening,Happening,PCs,_,_),
                     satisfieds(PCs,Sitn) ),	 % Es fisicamente factible
                Happenings),
        rnd_member(Happening, Happenings),      % elegir un suceso al azar
        do_event(Happening, Sitn, NewSitn),
        revise_goal(NewSitn, Goal, NewGoal),
        make_best_plan(NewGoal, NewSitn, NewActions),
        execute_plan(NewActions, NewSitn, NewGoal, 0, NextActions, NextSitns).

% ocurren eventos normales
execute_plan([Action|Actions], Sitn, Goal, P, [Action|NextActions], [Sitn|NextSitns]) :-
        do_event(Action, Sitn, NewSitn),
        execute_plan(Actions, NewSitn, Goal, P, NextActions, NextSitns).

do_event(Event, Situation, NewSituation) :-
        event_definition(_, Event, _PCs, Dels, Adds),
        apply_effects(Dels, Adds, Situation, NewSituation).

% Realizar las eliminaciones y adiciones según corresponda
apply_effects(Dels, Adds, Situation, NewSituation) :-
        removes(Dels, Situation, MidSituation),
        append(Adds, MidSituation, NewSituation).

% ======================================================================
%               Otras utilidades
% ======================================================================

satisfieds([], _).
satisfieds([F|Fs], S) :-
        satisfied(F, S),
        satisfieds(Fs, S).

satisfied(Fact, Situation) :-
        member(Fact, Situation).
satisfied(Fact, Situation) :-
        rule((Fact :- Facts)),                    % El hecho es una ramificacion del mundo
        satisfieds(Facts, Situation).

% ---------- Escribiendo...

lwrite([]).
lwrite([X|Xs]) :- write('       '), lwrite2(X), nl, lwrite(Xs).

lwrite2([]).
lwrite2([BitX|BitXs]) :- !, write(BitX), lwrite2(BitXs).
lwrite2(X) :- write(X).

anglify([], []).
anglify([Event|Events], [English|Englishs]) :-
        event_english(Event, English),
        anglify(Events, Englishs).

% ======================================================================
%               Utilidades generales
% ======================================================================

removes([], L, L).
removes([R|Rs], L, NewL) :-
        remove(R, L, MidL),
        removes(Rs, MidL, NewL).

remove(A, [A|B], B).
remove(A, [C|B], [C|NewB]) :-
        remove(A, B, NewB).

subset([], _).
subset([X|Xs], Ys) :- remove(X, Ys, RestYs), subset(Xs, RestYs).

nmember(Elem, List, N) :-
        nmember(Elem, List, 1, N).

nmember(Elem, [Elem|_], N, N).
nmember(Elem, [_|List], NSoFar, N) :-
        NewN is NSoFar + 1,
        nmember(Elem, List, NewN, N).

% ---------- Utilidades de aleatorización

:- dynamic lastrnd/1.
lastrnd(0).

maybe(P) :- random_float < P, !.		   % Exito con probabilidad P

rnd_member(X, Xs) :-
        length(Xs, L),
        R is random_float,
        N is integer(R*L) + 1,
        nmember(X, Xs, N), !.

% ======================================================================
%               EL INCIDENTE DE VUELO - BASE DE CONOCIMIENTO
% ======================================================================

event_definition(Type, Event, PCs, Adds, Dels) :- ed(Type, Event, PCs, Adds, Dels, _).
event_english(Event, English) :- ed(_, Event, _, _, _, English).

% ---------- Acciones rutina... ----------

ed(action, load(Passengers,Airplane),
        /*pcs*/ [plocation(Passengers,gate(Airport)),alocation(Airplane,gate(Airport))],
        /*del*/ [plocation(Passengers,gate(Airport))],
        /*add*/ [contains(Airplane,Passengers)],
        /*txt*/ 'The passengers boarded the plane.' ).

ed(action, taxi_to_runway(Airplane),
        /*pcs*/ [alocation(Airplane,gate(Airport))],
        /*del*/ [alocation(Airplane,gate(Airport))],
        /*add*/ [alocation(Airplane,runway(Airport))],
        /*txt*/ 'The plane taxiied to the runway.' ).

ed(action, take_off(Airplane,Airport),
        /*pcs*/ [alocation(Airplane,runway(Airport))],
        /*del*/ [alocation(Airplane,runway(Airport))],
        /*add*/ [alocation(Airplane,near(Airport))],
        /*txt*/ ['The plane took off from ',Airport,'.']).

ed(action, cruise(Airplane,Airport1,Airport2),
        /*pcs*/ [flight_path(Airport1,Airport2),alocation(Airplane,near(Airport1))],
        /*del*/ [alocation(Airplane,near(Airport1))],
        /*add*/ [alocation(Airplane,near(Airport2))],
        /*txt*/ ['The plane cruised towards ',Airport2,'.']).

ed(action, land(Airplane,Airport2),
        /*pcs*/ [alocation(Airplane,near(Airport2))],
        /*del*/ [alocation(Airplane,near(Airport2))],
        /*add*/ [alocation(Airplane,runway(Airport2))],
        /*txt*/ ['The plane landed at ',Airport2,'.']).

ed(action, taxi_to_gate(Airplane),
        /*pcs*/ [alocation(Airplane,runway(Airport))],
        /*del*/ [alocation(Airplane,runway(Airport))],
        /*add*/ [alocation(Airplane,gate(Airport))],
        /*txt*/ 'The plane taxiied to the gate.' ).

ed(action, unload(Passengers,Airplane),
        /*pcs*/ [contains(Airplane,Passengers),alocation(Airplane,gate(Airport))],
        /*del*/ [contains(Airplane,Passengers)],
        /*add*/ [plocation(Passengers,gate(Airport))],
        /*txt*/ 'The passengers disembarked.' ).

% ---------- Acciones de emergencia... ----------

ed(action, evacuate(Airplane),
        /*pcs*/ [a_on_ground(Airplane),alocation(Airplane,Loc),contains(Airplane,Passengers)],
        /*del*/ [contains(Airplane,Passengers)],
        /*add*/ [plocation(Passengers,Loc)],
        /*txt*/ 'The passengers were evacuated from the plane.' ).

ed(action, emergency_landing(Airplane),
        /*pcs*/ [alocation(Airplane,near(Airport2))],
        /*del*/ [alocation(Airplane,near(Airport2))],
        /*add*/ [alocation(Airplane,on_ground_near(Airport2))],
        /*txt*/ ['The pilot made an emergency landing near ',Airport2,'.']).

ed(action, medical_help(Passengers),
        /*pcs*/ [plocation(Passengers, gate(_))],               % cualquier puerta
        /*del*/ [],
        /*add*/ [medical_help(Passengers)],
        /*txt*/ 'Medical help was provided.' ).

% ---------- Sucesos posibles... ----------

ed(happening, fire(engine),
        /*pcs*/ [],                                     % puede suceder en cualquier lugar
        /*del*/ [],
        /*add*/ [on_fire(engine)],
        /*txt*/ 'The engine caught fire.' ).

% ---------- Sucesos posibles... ----------

ed(happening, ill_passenger,
        /*pcs*/ [contains(Airplane,Passengers),passengers(Passengers),airplane(Airplane)],
        /*del*/ [],
        /*add*/ [ill_passenger],
        /*txt*/ 'A passenger became very ill.' ).

%Ramificaciones de hechos sobre el mundo...
rule(( a_on_ground(Airplane) :- [alocation(Airplane,gate(_))] )).
rule(( a_on_ground(Airplane) :- [alocation(Airplane,runway(_))] )).
rule(( a_on_ground(Airplane) :- [alocation(Airplane,on_ground_near(_))] )).
rule(( p_on_ground(Passengers) :- [plocation(Passengers,gate(_))] )).
rule(( p_on_ground(Passengers) :- [plocation(Passengers,runway(_))] )).
rule(( p_on_ground(Passengers) :- [plocation(Passengers,on_ground_near(_))] )).

% Reglas para revisar el objetivo
revise_goal(Situation, plocation(Passengers,_), Goal) :- % Si la maquinaria esta prendida fuego,
        memberchk(on_fire(engine), Situation), !,   % Ir al piso rapido!
        Goal = p_on_ground(Passengers).
revise_goal(Situation, plocation(Passengers,_), Goal) :- % Si pasagero esta enfermo,
        memberchk(ill_passenger, Situation), !,	   % Ir a una puerta.
        Goal = medical_help(Passengers).
revise_goal(_Situation, Goal, Goal).



